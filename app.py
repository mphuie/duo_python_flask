import duo_web
from flask import Flask, render_template, request, redirect, url_for
from dotenv import load_dotenv
import os

# Load keys from .env file
load_dotenv('.env')

app = Flask(__name__)

@app.route('/')
def home():
  return redirect(url_for('auth_local'))

@app.route('/auth', methods=['GET', 'POST'])
def auth_local():
  if request.method == 'GET':
    return render_template('auth_local.html')

  if request.method == 'POST':

    AKEY = os.environ.get('AKEY')
    SKEY = os.environ.get('SKEY')
    IKEY = os.environ.get('IKEY')
    HOST = os.environ.get('HOST')

    # Show local login
    if not 'sig_response' in request.form:

      username = request.form['username']
      password = request.form['password']

      # validate username

      sign_function = duo_web.sign_request
      sig_request = sign_function(IKEY, SKEY, AKEY, username)
      return render_template('auth_duo.html', host=HOST, sig_request=sig_request)

    # Show duo auth
    else:
      sig_response = request.form['sig_response']
      user = duo_web.verify_response(IKEY, SKEY, AKEY, sig_response)
      print user
      if user:

        # See if it was a response to an ENROLL_REQUEST
        user = duo_web.verify_enroll_response(IKEY, SKEY, AKEY, sig_response)
        if user is None:
          return 'Did not authenticate with Duo', 403
        else:
          return 'Logged in with Duo as %s' % user

      else:
        return 'Something wrong!!!', 403

if __name__ == "__main__":
  app.run(debug=True)