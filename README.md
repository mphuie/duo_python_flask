This is a starter flask app that uses Duo for [2FA](https://duo.com/).  



## Usage

Install deps from `Pipfile.lock` - `pip install -p`

Create a `.env` file with these contents


```
AKEY=<a string that you generate and keep secret from Duo>
IKEY=<integration key from Duo>
SKEY=<secret key from Duo
HOST=<API hostname from Duo>
```

This python snippet can generate a random string for `AKEY`


```
#!python

import os, hashlib
print hashlib.sha1(os.urandom(32)).hexdigest()
```

Run `python app.py`